# Koohiisan's (Non-Magical) Minecraft Resource Pack

Modifications in version 1.8.6:

* MODIFIED: Wooden Sword is now Baseball Bat

Modifications in version 1.7.5:

* MODIFIED: Zombie Pigmen are now Nyan-Knights
* MODIFIED: Witches are now Angry Bartendermen who didn't get tipped properly and are ready to serve you drinks which you did not order
* MODIFIED: Corrected text and voice files

Modifications in version 1.6.2:

* MODIFIED: Graphics in place for Zombie Villagers, who are simply normal villagers wearing robot mind-control glasses
* MODIFIED: Replaced Zombie Villager sounds

Modifications in version 1.6.0:

* MODIFIED: Corrected text for Robot Archers (Skeletons/Strays)
* MODIFIED: Corrected text and sounds for Robots (Zombies/Husks)
* MODIFIED: Rotten Flesh is now Rotten Dog Treat (since that's what it's used for)

Modifications in version 1.5.2:

* MODIFIED: Skeletons are robotic archers (complete with quiver); Wither Skeletons are robotic swordsmen

Modifications in version 1.5.0:

* MODIFIED: Creepers are now reskinned Apple iCreeper devices bent on world domination (like in real life); their batteries are prone to overheating and explosions (also like in real life)
* MODIFIED: No more magical runes waft from bookshelves to Chemistry Sets (occasional electrostatically charged dust particles will appear to show that bookshelves are placed properly)

Modifications in version 1.4.6:

* MODIFIED: Dragon is now Dragonbot

Modifications in version 1.4.5:

* MODIFIED: Nyan-Cat Tears are now Nyan-Cat Teeth
* MODIFIED: added another Nyan-Cat sound effect
* MODIFIED: minor wording changes

Modifications in version 1.4:

* MODIFIED: Ghasts are now meowing, fire-breathing Nyan-Cats
* MODIFIED: Skeleton/Zombie Horses are tame velociraptors
* MODIFIED: Endermen have a flatulence problem
* MODIFIED: Elder Guardians have mild incontinence
* MODIFIED: Enchantment Tables are now Chemistry Sets with a Science Book (do it with science!)
* MODIFIED: Enchantments, Potions, *et al* are now science-y (e.g., flasks of chemical compounds crafted using science textbooks with a Chemistry Set are used in place of enchanted books)
* MODIFIED: Giants are colossal, robot invaders (requires plugin to spawn)
* MODIFIED: Wither is now Witherbot, a frenetic, destructive machine
* MODIFIED: Soul Sand is Sole Sand, which slows your steps and looks less macabre
* MODIFIED: Orange Tabby Cats are now Tortoiseshell Cats
* MODIFIED: Bats are now costumed
* Plugins recommended: DynMap, DropSwap, Giants, SmartGiants, Multiverse-Core, PerfectBackup, WorldGuard/WorldEdit, Diving_Helmet